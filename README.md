# djangular

DjangoのモデルからAngularのソースコードを出力するDjangoコマンド。


## 出来ること

* DjangoのモデルからAngularのソースコード出力。
* 各コマンドは `python manage.py コマンド名 アプリケーション名` で使用することが出来る。


## Djangoコマンド一覧

* `create_angular_model` - Angularのモデルを作成する。
* `create_angular_form_service` - 作成画面のAngularのformを提供するサービスを作成する。
* `create_angular_template` - 作成画面のAngularのテンプレートを作成する。


## Install方法

```
pip install git+https://gitlab.com/takahiro652c/djangular
```

* 使用するプロジェクトのsettings.pyのINSTALLED_APPSに `'djangular'` を追加。


## 試す

* 以下の方法でコマンドを試すことが出来る。

```
git clone https://gitlab.com/takahiro652c/djangular.git
cd djangular
pip install .
cd example
python manage.py コマンド名 example
```


## 注意

* 生成されるコードだけではAngularアプリとして動作しない。
* また、間違ったコードが生成される場合があるため、必ず目を通すこと。
* FIXMEやTODOコメントが生成されることがあり、その部分は必ず修正すること。


## 動作確認済み

### Field

* [このIsuue](https://gitlab.com/takahiro652c/djangular/issues/15)
のチェックボックスがついているものは動作確認済み。


## 今後の予定

* `create_angular_project` - 他のコマンドをすべて用いてAngularのプロジェクトを作成するコマンドの作成
* `create_django_serializer` - SerializerClassを作成するコマンドの作成
* jsonファイルなどで設定を読み込む機能の実装
* version情報追加
* CI設定


## 開発方針

### 理念

* あくまでDjango＋AngularのWebアプリ開発の補助ツールとして開発する。
つまり、完璧なコード生成を目的としない。
* また、提供するDjangoコマンドはWebアプリ開発時に数回のみ実行されると想定されるため、
コード生成の処理時間は重要視しない。
* README（このファイル）に情報を記述してから機能開発を行う。

### コメント出力

* 間違ったコードを10%以上の確率で生成するときはTODOコメントを出力する。
* 間違ったコードを20%以上の確率で生成するときはFIXMEコメントを出力する。

### コマンドを試しながら開発する方法

* プロジェクトのルートディレクトリ上で以下を実行。

```
pip install . && \
python example/manage.py コマンド名 example ; \
pip uninstall djangular --yes
```
