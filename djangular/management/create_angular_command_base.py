import os
import re

from django.core.management.base import AppCommand
from django.db import DEFAULT_DB_ALIAS


class CreateAngularCommandBase(AppCommand):

    default_output_path = './djangular_output/'

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            '--database',
            dest='database',
            default=DEFAULT_DB_ALIAS,
            help='データベースを指定する。'
                 '(Default: {})'.format(DEFAULT_DB_ALIAS)
        )
        parser.add_argument(
            '--output_path',
            dest='output_path',
            default=self.default_output_path,
            help='ファイルを出力するディレクトリを指定する。'
                 '(Default: {})'.format(self.default_output_path)
        )

    def handle_output_path(self, output_path):
        """
        output_pathの最後に '/' をつける
        output_pathが存在しなければmkdirする
        """
        if not output_path.endswith('/'):
            output_path += '/'

        if not os.path.exists(output_path):
            os.mkdir(output_path)

    def file_write(self, file_instance, str_write):
        """file_instanceにstr_writeと改行コードを書き込んだ後、str_writeをprintする"""
        file_instance.write(str_write + '\n')
        print(str_write)

    def get_file_name(self, model_name):
        """model_nameを元にファイル名を返す"""
        return self.convert_to_kebab_case(model_name)

    def print_file_path(self, file_path):
        """file_pathを出力する"""
        print()
        print('==========')
        print(file_path)
        print('==========')
        print()

    first_cap_re = re.compile('(.)([A-Z][a-z]+)')
    all_cap_re = re.compile('([a-z0-9])([A-Z])')

    def convert_to_kebab_case(self, name):
        """
        CamelCaseをkebab-caseに変換する
            https://stackoverflow.com/questions/1175208/
            elegant-python-function-to-convert-camelcase-to-snake-case
        """
        s1 = self.first_cap_re.sub(r'\1-\2', name)
        return self.all_cap_re.sub(r'\1-\2', s1).lower()

    postgresql_number_types_re = re.compile(
        '^(smallint|'
        'integer|'
        'bigint|'
        'decimal|'
        'numeric|'
        'real|'
        'double precision|'
        'smallserial|'
        'serial|'
        'bigserial)'
    )
    postgresql_string_types_re = re.compile(
        '^(character|'
        'varchar|'
        'character|'
        'char|'
        'text)'
    )
    postgresql_Date_types_re = re.compile(
        '^(timestamp|'
        'date|'
        'time|'
        'interval)'
    )
    postgresql_boolean_types_re = re.compile(
        '^boolean'
    )

    def convert_database_type_to_typescript_type(self, str_type):
        """database上の型を表す文字列をtypescript上の型を表す文字列に変換する"""
        try:
            if self.postgresql_number_types_re.match(str_type):
                return 'number'
            if self.postgresql_string_types_re.match(str_type):
                return 'string'
            if self.postgresql_Date_types_re.match(str_type):
                return 'Date'
            if self.postgresql_boolean_types_re.match(str_type):
                return 'boolean'
            return str_type
        except TypeError:
            return ''

    def convert_typescript_null_boolean(self, value):
        """PythonのboolとNoneをTypescriptのbooleanとnullに変換する"""
        if value is None:
            return 'null'
        if not type(value) == bool:
            return None
        if value:
            return 'true'
        return 'false'

    def is_required_field(self, field):
        """fieldがrequiredかどうか判定する"""
        # FIXME: requiredの判定
        if hasattr(field, 'blank'):
            return False
        if hasattr(field, 'has_default') and field.has_default():
            return False
        return True
