from django.db import connections, models

from djangular.management.create_angular_command_base import \
    CreateAngularCommandBase


class Command(CreateAngularCommandBase):
    help = 'Angularの作成画面のhtmlを作成するコマンド。'
    default_output_path = './djangular_output/'
    default_max_length_radio_choices = 5

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            '--check_required',
            dest='all_fields_required',
            action='store_false',
            help='それぞれのフィールドを必須か判定する。'
                 '指定しない場合はすべてのフィールドを必須にする。'
        )
        parser.add_argument(
            '--max_length_radio_choices',
            dest='max_length_radio_choices',
            default=self.default_max_length_radio_choices,
            help='choicesがあるフィールドについて、ラジオボタンにするか選択肢にするかのしきい値。'
                 'len(choices) > max_length_radio_choicesならば選択肢にする。'
                 '(Default: {})'.format(self.default_max_length_radio_choices)
        )

    def handle_app_config(self, app_config, **options):
        if app_config.models_module is None:
            return

        connection = connections[options.get('database')]
        app_models = app_config.get_models()

        all_fields_required = options.get('all_fields_required')
        max_length_radio_choices = options.get('max_length_radio_choices')

        output_path = options.get('output_path')
        self.handle_output_path(output_path)

        for model in app_models:
            model_name = model.__name__
            file_name = self.get_file_name(model_name)
            file_path = self.get_file_path(output_path, file_name)

            self.print_file_path(file_path)

            f = open(file_path, 'w')

            # models.Fieldのフィールド
            fields = model._meta.get_fields()
            for field in fields:
                self.write_form_item(
                    f,
                    field,
                    model_name,
                    connection,
                    all_fields_required,
                    max_length_radio_choices,
                )

            f.close

    def get_file_path(self, output_path, file_name):
        """出力するファイルのpathを返す"""
        return '{}{}.component.html'.format(output_path, file_name)

    content_tag_base = '<{tag_name} name="{name}" label="{label}"{attrs} '\
        '[required]="{required}" [form]="form" [hasTried]="hasTried">'\
        '</{tag_name}>'
    content_radio_tag = content_tag_base.format(
        tag_name='app-shared-radio',
        attrs=' [choices]="{choices}"',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_boolean_tag = content_radio_tag.format(
        choices='yesOrNoChoices',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_select_tag = content_tag_base.format(
        tag_name='app-shared-select',
        attrs=' [choices]="{choices}"',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_input_tag = content_tag_base.format(
        tag_name='app-shared-input',
        attrs=' type="{type}"',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_string_tag = content_input_tag.format(
        tag_name='app-shared-input',
        type='string',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_number_tag = content_input_tag.format(
        tag_name='app-shared-input',
        type='number',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_email_tag = content_input_tag.format(
        tag_name='app-shared-input',
        type='email',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_date_picker_tag = content_tag_base.format(
        tag_name='app-shared-date-picker',
        attrs='',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_datetime_picker_tag = content_tag_base.format(
        tag_name='app-shared-datetime-picker',
        attrs='',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_textarea_tag = content_tag_base.format(
        tag_name='app-shared-textarea',
        attrs='',
        name='{name}',
        label='{label}',
        required='{required}',
    )
    content_others_tag = '''<!-- TODO: 正しいフォーム要素を記述 -->
<!-- {} -->'''.format(content_input_tag.format(
        type='タイプ',
        name='{name}',
        label='{label}',
        required='{required}',
    ))

    def write_form_item(
        self,
        f,
        field,
        model_name,
        connection,
        all_fields_required,
        max_length_radio_choices,
    ):
        """フォームの一つの要素を出力する"""
        str_type = self.convert_database_type_to_typescript_type(
            field.db_type(connection)
        )
        name = field.name

        label = ''
        if hasattr(field, 'verbose_name'):
            label = field.verbose_name

        required = self.convert_typescript_null_boolean(
            all_fields_required or self.is_required_field(field)
        )

        if hasattr(field, 'choices') and len(field.choices) > 0:
            choices = '{}.{}_CHOICES'.format(model_name, name.upper())
            if len(field.choices) > max_length_radio_choices:
                self.file_write(f, self.content_select_tag.format(
                    name=name,
                    label=label,
                    choices=choices,
                    required=required
                ))
            else:
                self.file_write(f, self.content_radio_tag.format(
                    name=name,
                    label=label,
                    choices=choices,
                    required=required
                ))
        elif str_type == 'boolean':
            self.file_write(f, self.content_boolean_tag.format(
                name=name,
                label=label,
                required=required
            ))
        elif str_type == 'number':
            self.file_write(f, self.content_number_tag.format(
                name=name,
                label=label,
                required=required
            ))
        elif str_type == 'Date':
            if isinstance(field, models.DateTimeField):
                self.file_write(f, self.content_datetime_picker_tag.format(
                    name=name,
                    label=label,
                    required=required
                ))
            else:
                self.file_write(f, self.content_date_picker_tag.format(
                    name=name,
                    label=label,
                    required=required
                ))
        elif isinstance(field, models.TextField):
            self.file_write(f, self.content_textarea_tag.format(
                name=name,
                label=label,
                required=required
            ))
        elif isinstance(field, models.EmailField):
            self.file_write(f, self.content_email_tag.format(
                name=name,
                label=label,
                required=required
            ))
        elif str_type == 'string':
            self.file_write(f, self.content_string_tag.format(
                name=name,
                label=label,
                required=required
            ))
        else:
            self.file_write(f, self.content_others_tag.format(
                name=name,
                label=label,
                required=required
            ))
