import datetime
from decimal import Decimal

from django.db import connections
from django.utils.itercompat import is_iterable

from djangular.management.create_angular_command_base import \
    CreateAngularCommandBase


class Command(CreateAngularCommandBase):
    help = 'Angularのmodelを作成するコマンド。'
    default_output_path = './djangular_output/'
    content_head = """import {{ ModelBase }} from '@app/core/models/model-base';
import {{ Choice }} from '@app/shared/models';

/**
 * @classdesc {verbose_name}
 * @export
 * @class {model_name}
 * @extends {{ModelBase<{model_name}>}}
 */
export class {model_name} extends ModelBase<{model_name}> {{"""

    content_foot = """
  constructor(src?: {model_name}) {{
    super();
    this.setValues(src);
  }}
}}"""

    def add_arguments(self, parser):
        super().add_arguments(parser)

    def handle_app_config(self, app_config, **options):
        if app_config.models_module is None:
            return

        connection = connections[options.get('database')]
        models = app_config.get_models()

        output_path = options.get('output_path')
        self.handle_output_path(output_path)

        for model in models:
            model_name = model.__name__
            model_verbose_name = model._meta.verbose_name
            file_name = self.get_file_name(model_name)
            file_path = self.get_file_path(output_path, file_name)

            self.print_file_path(file_path)

            f = open(file_path, 'w')
            self.file_write(
                f,
                self.content_head.format(
                    model_name=model_name,
                    verbose_name=model_verbose_name
                )
            )

            self.write_static_fields(f, model, model_name)
            self.write_instance_fields(f, model, connection)

            self.file_write(f, self.content_foot.format(
                model_name=model_name
            ))
            f.close

    def get_file_path(self, output_path, file_name):
        """出力するファイルのpathを返す"""
        return '{}{}.ts'.format(output_path, file_name)

    str_static_field = '  static {key} = {value};'

    def write_static_fields(self, f, model, model_name):
        """
        Angularでstaticフィールドとなるフィールドを出力する
        （Djangoのmodels.Field以外のフィールド）
        """
        for key, value in vars(model).items():
            if self.is_static_field(key, value):
                if self.is_choices(value):
                    self.file_write(
                        f,
                        self.convert_to_angular_choices(key, value, model_name)
                    )
                else:
                    self.file_write(f, self.str_static_field.format(
                        key=key,
                        value=value
                    ))

    str_instance_field = '  {key}: {type} = null;'

    def write_instance_fields(self, f, model, connection):
        """
        Angularでインスタンスのフィールドとなるフィールドを出力する
        （Djangoのmodels.Fieldのフィールド）
        """
        fields = model._meta.get_fields()
        for field in fields:
            str_type = self.convert_database_type_to_typescript_type(
                field.db_type(connection)
            )
            self.file_write(f, self.str_instance_field.format(
                key=field.name,
                type=str_type
            ))

    not_static_field_keys = ('__module__', '__doc__')
    static_field_types = (
        int,
        float,
        str,
        bool,
        tuple,
        list,
        dict,
        Decimal,
    )

    def is_static_field(self, key, field):
        """staticなfieldかどうか判定する"""
        return type(field) in self.static_field_types \
            and key not in self.not_static_field_keys

    def is_choices(self, choices):
        """choicesかどうか判定する"""
        if isinstance(choices, str) or not is_iterable(choices):
            return False
        elif any(isinstance(choice, str) or
                 not is_iterable(choice) or len(choice) != 2
                 for choice in choices):
            return False
        return True

    number_types = (
        int,
        float,
        Decimal,
    )
    boolean_types = (bool,)
    string_types = (str,)
    Date_types = (datetime.date, datetime.datetime)

    def convert_python_type_to_typescript_type(self, value):
        """pythonの型をtypescript上の型を表す文字列に変換する"""
        value_type = type(value)
        if value_type in self.number_types:
            return 'number'
        elif value_type in self.boolean_types:
            return 'boolean'
        elif value_type in self.string_types:
            return 'string'
        elif value_type in self.Date_types:
            return 'Date'
        return value_type

    str_choices_head = '''
  // TODO: valueを対応する定数名に置き換える。
  static {choices_name}: Choice<{type}>[] = [
'''
    str_choice = "    {{ value: {model_name}.{value}, label: '{label}' }},\n"

    def convert_to_angular_choices(self, key, choices, model_name):
        """DjangoのモデルのchoicesをAngularのモデルのchoicesを表す文字列に変換する"""
        str_choices = self.str_choices_head.format(
            choices_name=key,
            type=self.get_choices_typescript_type(choices)
        )
        for choice in choices:
            str_choices += self.str_choice.format(
                value=choice[0],
                label=choice[1],
                model_name=model_name
            )

        str_choices += '  ];\n'
        return str_choices

    def get_choices_typescript_type(self, choices):
        """Djangoのモデルのchoicesのtypescriptの型を類推し、結果を返す"""
        for choice in choices:
            if choice[0] is not None:
                return self.convert_python_type_to_typescript_type(choice[0])
        return self.convert_python_type_to_typescript_type(None)
