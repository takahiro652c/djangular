from djangular.management.create_angular_command_base import \
    CreateAngularCommandBase


class Command(CreateAngularCommandBase):
    help = 'Angularの作成画面のFormServiceを作成するコマンド。'
    default_output_path = './djangular_output/'

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            '--check_required',
            dest='all_fields_required',
            action='store_false',
            help='それぞれのフィールドを必須か判定する。'
                 '指定しない場合はすべてのフィールドを必須にする。'
        )

    def handle_app_config(self, app_config, **options):
        if app_config.models_module is None:
            return

        app_models = app_config.get_models()

        all_fields_required = options.get('all_fields_required')

        output_path = options.get('output_path')
        self.handle_output_path(output_path)

        for model in app_models:
            model_name = model.__name__
            model_label = model._meta.verbose_name
            file_name = self.get_file_name(model_name)
            file_path = self.get_file_path(output_path, file_name)

            self.print_file_path(file_path)

            f = open(file_path, 'w')

            self.write_content(
                f,
                model,
                model_name,
                model_label,
                file_name,
                all_fields_required,
            )

            f.close

    def get_file_path(self, output_path, file_name):
        """出力するファイルのpathを返す"""
        return '{}{}-form.service.ts'.format(output_path, file_name)

    content_import = """import {{ Injectable }} from '@angular/core';
import {{ FormBuilder, FormGroup, Validators }} from '@angular/forms';
import {{ {model_name} }} from '@app/core/models/{file_name}';
import {{ FormBaseService }} from '@app/shared/form/form-base.service';
"""

    content_class_document = """/**
 * {model_label}のformに関する処理を行う。
 * @export
 * @class {model_name}FormService
 * @extends {{FormBaseService<{model_name}>}}
 */"""

    content_class_head = """@Injectable()
export class {model_name}FormService extends FormBaseService<{model_name}> {{
  constructor(fb: FormBuilder) {{
    super(fb);
  }}

  /**
   * {model_label}のformを返す。
   * @param {{{model_name}}} src 初期値設定用のインスタンス。
   * @memberof {model_name}FormService
   */
  getForm(src: {model_name}): FormGroup {{
    const group = {{"""

    content_class_foot = """    }};

    return super.getForm(group);
  }}
}}"""

    def write_content(
        self,
        f,
        model,
        model_name,
        model_label,
        file_name,
        all_fields_required,
    ):
        """ファイルにFormServiceを出力する"""
        self.file_write(f, self.content_import.format(
            model_name=model_name,
            file_name=file_name
        ))
        self.file_write(f, self.content_class_document.format(
            model_name=model_name,
            model_label=model_label
        ))
        self.file_write(f, self.content_class_head.format(
            model_name=model_name,
            model_label=model_label
        ))

        fields = model._meta.get_fields()
        for field in fields:
            self.write_form_control(f, field, all_fields_required)

        self.file_write(f, self.content_class_foot.format(
        ))

    content_form_control = '      {name}: {value},'
    content_validator_required = 'Validators.required'
    content_validator_max = 'Validators.max({max})'
    content_validator_min = 'Validators.min({min})'
    content_validator_maxLength = 'Validators.maxLength({max})'
    content_validator_minLength = 'Validators.minLength({min})'
    content_validator_email = 'Validators.email'
    content_validator_isInteger = 'isIntegerValidator'

    def write_form_control(self, f, field, all_fields_required):
        """FormControlを表すオブジェクトの要素を出力する"""
        default_value = self.get_default_value(field)
        validators = self.get_validators(field, all_fields_required)

        validators_length = len(validators)
        if validators_length == 0:
            value = default_value
        elif validators_length == 1:
            value = '[{}, {}]'.format(default_value, validators[0])
        else:
            str_validators = ', '.join(validators)
            value = '[{}, [{}]]'.format(default_value, str_validators)

        self.file_write(f, self.content_form_control.format(
            name=field.name,
            value=value
        ))

    def get_default_value(self, field):
        """FormControlのdefault値を返す"""
        if hasattr(field, 'has_default') and field.has_default():
            value = field.default
            bool_value = self.convert_typescript_null_boolean(value)
            if bool_value is not None:
                return bool_value
            return value
        return 'null'

    def get_validators(self, field, all_fields_required):
        """FormControlのvalidatorのリストを返す"""
        validators = []

        if all_fields_required or self.is_required_field(field):
            validators.append(self.content_validator_required)

        if hasattr(field, 'max_length') and field.max_length is not None:
            validators.append(self.content_validator_maxLength.format(
                max=field.max_length
            ))

        return validators
