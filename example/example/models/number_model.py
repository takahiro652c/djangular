from decimal import Decimal

from django.db import models


class NumberModel(models.Model):

    class Meta():
        verbose_name = 'typescriptでnumber型になるフィールドをもつモデル'

    big_integer_field = models.BigIntegerField('BigIntegerフィールド')
    decimal_field = models.DecimalField(
        'Decimalフィールド',
        max_digits=5,
        decimal_places=2
    )
    float_field = models.FloatField('Floatフィールド')
    integer_field = models.IntegerField('Integerフィールド')
    positive_integer_field = models.PositiveIntegerField(
        'PositiveIntegerフィールド'
    )
    positive_small_integer_field = models.PositiveIntegerField(
        'PositiveSmallIntegerフィールド'
    )
    small_integer_field = models.SmallIntegerField('SmallIntegerフィールド')

    # 以下オプションがあるフィールド
    integer_default_field = models.IntegerField(
        'Integerフィールド（default=0）',
        default=0
    )

    INTEGER_1 = 1
    INTEGER_2 = 2
    INTEGER_CHOICES = (
        (INTEGER_1, 'Integer選択肢1'),
        (INTEGER_2, 'Integer選択肢2'),
    )
    integer_choices_field = models.IntegerField(
        'Integerフィールド（choices=INTEGER_CHOICES）',
        choices=INTEGER_CHOICES
    )

    INTEGER_CHOICES2 = [
        (val, 'Integer選択肢2の{}'.format(val)) for val in range(1, 20)
    ]
    integer_choices2_field = models.IntegerField(
        'Integerフィールド（choices=INTEGER_CHOICES2）',
        choices=INTEGER_CHOICES
    )

    FLOAT_1 = 1.1
    FLOAT_2 = 1.2
    FLOAT_CHOICES = (
        (FLOAT_1, 'Float選択肢1'),
        (FLOAT_2, 'Float選択肢2'),
    )
    float_choices_field = models.FloatField(
        'Floatフィールド（choices=FLOAT_CHOICES）',
        choices=FLOAT_CHOICES
    )

    DECIMAL_1 = Decimal(1)
    DECIMAL_2 = Decimal('2.5')
    DECIMAL_CHOICES = (
        (DECIMAL_1, 'Decimal選択肢1'),
        (DECIMAL_2, 'Decimal選択肢2'),
    )
    decimal_choices_field = models.DecimalField(
        'Decimalフィールド（choices=DECIMAL_CHOICES）',
        choices=DECIMAL_CHOICES,
        max_digits=2,
        decimal_places=1
    )
