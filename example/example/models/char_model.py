from django.db import models


class CharModel(models.Model):

    class Meta():
        verbose_name = 'Charフィールドをもつモデル'

    char_field = models.CharField('Charフィールド', max_length=50)
    blank_char_field = models.CharField(
        'Charフィールド（blank=True）', max_length=50, blank=True
    )
