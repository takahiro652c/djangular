
from django.db import models


class OthersModel(models.Model):

    class Meta():
        verbose_name = 'その他のモデル'

    NOT_CHOICES = (1, 2, 3, 4)
