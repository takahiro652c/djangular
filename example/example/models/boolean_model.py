from django.db import models


class BooleanModel(models.Model):

    class Meta():
        verbose_name = 'typescriptでboolean型になるフィールドをもつモデル'

    boolean_field = models.BooleanField('Booleanフィールド')
    null_boolean_field = models.NullBooleanField(
        'NullBooleanフィールド'
    )

    # 以下オプションがあるフィールド
    boolean_default_field = models.BooleanField(
        'Booleanフィールド（default=False）',
        default=False
    )
    null_boolean_default_field = models.NullBooleanField(
        'NullBooleanフィールド（default=None）',
        default=None
    )
    null_boolean_blank_field = models.NullBooleanField(
        'NullBooleanフィールド（blank=True）',
        blank=True
    )

    BOOLEAN_CHOICES = (
        (True, 'True選択肢'),
        (False, 'False選択肢'),
    )
    boolean_choices_field = models.BooleanField(
        'Booleanフィールド（choices=BOOLEAN_CHOICES）',
        choices=BOOLEAN_CHOICES
    )

    NULL_BOOLEAN_CHOICES = (
        (None, 'None選択肢'),
        (True, 'True選択肢'),
        (False, 'False選択肢'),
    )
    null_boolean_choices_field = models.NullBooleanField(
        'NullBooleanフィールド（choices=NULL_BOOLEAN_CHOICES）',
        choices=NULL_BOOLEAN_CHOICES
    )
