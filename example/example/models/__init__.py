from example.models.boolean_model import BooleanModel
from example.models.char_model import CharModel
from example.models.date_model import DateModel
from example.models.number_model import NumberModel
from example.models.relation_model import RelationModel
from example.models.others_model import OthersModel
