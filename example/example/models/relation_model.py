from django.db import models

from example.models.example_foreign_key import ExampleForeignKey


class RelationModel(models.Model):

    class Meta():
        verbose_name = 'リレーションフィールドをもつモデル'

    foreign_key = models.ForeignKey(ExampleForeignKey)
