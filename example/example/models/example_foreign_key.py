from django.db import models


class ExampleForeignKey(models.Model):

    class Meta():
        verbose_name = '外部キーのモデルの例'
