import datetime

from django.db import models


class DateModel(models.Model):

    class Meta():
        verbose_name = 'typescriptでDate型になるフィールドをもつモデル'

    date_field = models.DateField('Dateフィールド')
    date_time_field = models.DateTimeField('DateTimeフィールド')

    # 以下オプションが有るフィールド
    date_null_blank_field = models.DateField(
        "Dateフィールド（null=Ture, blank=True）", null=True, blank=True
    )
    date_time_auto_now_field = models.DateTimeField(
        "DateTimeフィールド（auto_now=True）", auto_now=True
    )
    date_time_auto_now_add_field = models.DateTimeField(
        "DateTimeフィールド（auto_now_add=True）", auto_now=True
    )

    DATE_1 = datetime.date(2000, 1, 1)
    DATE_2 = datetime.date(2000, 1, 2)
    DATE_CHOICES = (
        (DATE_1, 'DATE選択肢1'),
        (DATE_2, 'DATE選択肢2'),
    )
    date_choices_field = models.DateField(
        'Dateフィールド（choices=DATE_CHOICES）', choices=DATE_CHOICES
    )

    DATETIME_1 = datetime.datetime(2000, 1, 1, 1, 1, 1)
    DATETIME_2 = datetime.datetime(2000, 1, 1, 1, 1, 2)
    DATETIME_CHOICES = (
        (DATETIME_1, 'DATETIME選択肢1'),
        (DATETIME_2, 'DATETIME選択肢2'),
    )
    datetime_choices_field = models.DateTimeField(
        'DateTimeフィールド（choices=DATETIME_CHOICES）',
        choices=DATETIME_CHOICES
    )
